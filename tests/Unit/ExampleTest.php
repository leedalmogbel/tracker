<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    /**@test */
    public function test_index_page_exist()
    {
        // $this->assertTrue(true);

        $response = $this->get('/');

        $response->assertSee('Tracker');
    }

    /**@test */
    public function test_search_tracker_code()
    {
        $response = $this->json('GET', '/api/shippingtracker/111', ['tracking_code' => '111']);

        $response
            ->assertStatus(200)
            ->assertJson([
                'delivery_code' => '2019-05-03 13:00:00',
            ]);
    }
}
