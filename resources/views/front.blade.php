@extends('layout')

@section('content')
<!-- -->
<div class="container mt-5">
    <div class="col-md-12 col-centered">
        <form id="tracking-form" method="GET">
            <div class="form-group">
                <div class="input-group input-group-lg">
                    <input class="form-control" placeholder="Enter tracking number" type="text" value="">
                    <span class="input-group-btn">
                        <button class="btn btn-outline-secondary" type="submit">
                            <div class="fa fa-binoculars"></div>
                            <span class="hidden-xs">
                                Track package
                            </span>
                        </button>
                    </span>
                </div>
            </div>
        </form>
        <div id="deliver"></div>
    </div>
</div>
<!-- -->
<script>
    $('#tracking-form').submit(function (e) {
        e.preventDefault();
        var input = $(this).find('input[type="text"]')
        input.removeClass('is-invalid');
        var val = input.val();
        if (val.trim() == '' || val == null || typeof val == 'undefined') {
            var div = "<div class='text-danger'><strong>Please provide a tracking number.</strong></div>";
            $('#deliver').html(div);
            input.addClass('is-invalid');
            return;
        }

        $.get('/api/shippingtracker/' + val, function (res) {
            if (typeof res.delivery_date == 'undefined') {
                var div = "<div class='text-danger'><strong>Something went wrong.</strong></div>";
                $('#deliver').html(div);
                input.addClass('is-invalid');
                return;
            }

            var div = "<div>Your order will be delivered on <strong>" + res.delivery_date + ".</strong></div>";
            $('#deliver').html(div);
        }).fail(function() {
            var div = "<div class='text-danger'><strong>Cannot find your tracking number.</strong></div>";
            $('#deliver').html(div);
            input.addClass('is-invalid');
            return;
        })
    })
</script>
@endsection