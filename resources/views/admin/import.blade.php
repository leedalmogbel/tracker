@extends('layout')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">Import CSV</div>
        <div class="card-body">
            @if (Session::has('__errs'))
                @foreach (Session::get('__errs') as $err)
                    <div class="text-danger">{{ $err }}</div>
                @endforeach
            @endif

            @if (Session::has('__succ'))
                @foreach (Session::get('__succ') as $succ)
                    <div class="text-success">{{ $succ }}</div>
                @endforeach
            @endif
            <br />
            <form method="POST" enctype="multipart/form-data">
                @csrf
                <input type="file" name="csv_file" id="import-csv">
                <button type="submit" class="btn-primary">Import</button>
            </form>
        </div>
    </div>
</div>
@endsection
