# Shipping Code Tracker

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them

```
PHP 7.1^
Laravel 5.6^
Mysql
Composer
PHPUnit
```

### Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
create Database named [tracking]

this need [composer install]

rename [.env.example] to [.env]

change [DB_DATABASE] [DB_USERNAME] [DB_PASSWORD]

generate hash [php artisan key:generate]

use command [php artisan migrate] for database 

seed database to fill up [php artisan db:seed]

and to run a local server [php artisan serve] which may set [127.0.0.1:8000] as default


for unit test [vendor/bin/phpunit]
```
