<?php

namespace App\Http\Controllers;

use Request;
use App\Shipping;
use Session;

class AdminController extends Controller
{
    //

    public function importCSV () {
        // get file from request
        $file = Request::file('csv_file');

        // check if file is uploaded
        if (empty($file)) {
            Session::flash('__errs', ['Please select/upload csv file.']);
            return redirect('/admin/import');
        }
        // get file data
        $data = $file->get();
        // check if file is a valid csv file
        if ($file->getClientOriginalExtension() != 'csv') {
            // set an error and redirect
            Session::flash('__errs', ['File is not a CSV file']);
            // redirect back to form
            return redirect('/admin/import');
        }

        // split data with newline
        $data = explode("\n", $data);

        // parse csv data from string
        $heading = str_getcsv($data[0]);

        // validate headings
        if (strtolower(trim($heading[0])) != 'tracking_code' || strtolower(trim($heading[1])) != 'delivery_date') {
            Session::flash('__errs', ['Invalid CSV']);
            return redirect('/admin/import');
        }
        // unset heading from csv data
        unset($data[0]);

        $errors = [];
        $success = [];

        // loop though csv data
        foreach ($data as $k => $entry) {
            // parse csv from string
            $columns = str_getcsv($entry);

            // check if required columns are set
            if (!isset($columns[0]) || !isset($columns[1])) {
                $errors[] = 'Invalid data on row #' . $k; 
                continue;
            }

            try {
                // update database from csv
                Shipping::updateFromCsv($columns[0], $columns[1]);
                // add to success message
                $success[] = 'tracking number ' . $columns[0] . ' successfully updated';
            } catch (\Throwable $e) {
                // if something goes wrong add to error messages
                $errors[] = $e->getMessage() . ': Row #' . $k; 
                continue;
            }
        }

        // set errors and success to session
        Session::flash('__errs', $errors);
        Session::flash('__succ', $success);
        // redirect back to form
        return redirect('/admin/import');
    }

    
    public function import () {
        // display view admin import
        return view('admin.import');
    }
}
