<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    //
    public static function updateFromCsv($code, $date) {
        // get shipping by tracking_code
        $shipping = self::where('tracking_code', '=', $code)->firstOrFail();

        // if shipping is empty 
        if (empty($shipping)) {
            // throw an exception
            throw new \Exception('Tracking number not found');
        }
        
        // check if datestring is valid
        if (!@strtotime($date)) {
            // throw exception
            throw new \Exception('Invalid date');
        }

        // set new shipping date
        $shipping->delivery_date = date('Y-m-d H:i:s', strtotime($date));
        // save to database
        $shipping->save();
    }
}
